#we can store basic data using dbm#but complex objects need another approach.
#pickling is the way to accomplish this.
import pickle

countries = {"in":{"India":["Mumbai","Pune","Delhi"]},"jp":{"Japan":["Tokio","Heroshima","Yakohama"]}}
ds = open("datastore","wb")

store = pickle.dump(countries,ds)
