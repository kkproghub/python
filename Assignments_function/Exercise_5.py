#Just as functions take raw material like a machine,
#they can give output in terms of a finished product as well.
#This finished product is called a return value.
#return keyword is used for this.
#Return is always the last statement in the function.

def area(l,b):
    result = l * b
    return result
finalresult = area(8,4.2)

print(finalresult)
#print(area(5,5))
