#function can execute set of instructions at more than once place without rewriting the code.
#def is the keyword to start a new function.
def print_lyrics():
    print("I'm a lumberjack, and I'm okay.")
    print("I sleep all night and I work all day.")



print_lyrics()
