#!/usr/bin/python3
import math
num = int(input("please input a number "))
valCeil = math.ceil(num)
print("ceil of %d is %d "%(num, valCeil))

valFloor = math.floor(num)
print("floor of %d is %d "%(num, valFloor))
valFactorial = math.factorial(num)
print("factorial of %d is %d "%(num, valFactorial))
valSqrt = math.sqrt(num)
print("sqrt of %d is %d "%(num, valSqrt))
