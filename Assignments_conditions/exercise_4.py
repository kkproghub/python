"""
When true or false depends on more than 1 factor.
E.g student will get admission in computer science only if he/she is older than 18 and has taken math in last year.
"""
print("students can take computer science only if they have taken math in last year and are above 18 ")
subject = input("which was your last subject? mt for math and st for stats")
age = int(input("what's your age ?"))
if subject == "mt" and age >= 18:
    print("you are eligible")
else:
    print("you are not eligible ")
