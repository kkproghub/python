"""
When true or false depends on one of the two conditions to be true.
Can involve more than 2 conditions.
E.g student will get admission in computer science either if he/she is older than 18 or has taken math in last year.
"""
print("students can take computer science  if they have either taken math in last year or are above 18 ")
subject = input("which was your last subject? mt for math and st for stats")
age = int(input("what's your age ?"))
if subject == "mt" or age >= 18:
    print("you are eligible")
else:
    print("you are not eligible ")
