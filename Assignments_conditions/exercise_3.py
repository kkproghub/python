"""
Multiple conditions using elif.
In this case we know that there are multiple possibilities so there are many branches of execution.
Checking a grade of student depending on average.
75% or more is distinction.
60 or more is first grade.
50 or more is second grade.
35 or more is passing grade.

"""

grade =int(input("please enter the marks in percent "))
if grade >= 75:
    print("you got distinction ")
    grade = 0
elif grade >= 60:
    print("you are in first grade ")
    grade = 0
elif grade >= 50:
    print("you are in second grade")
    grade = 0
elif grade >=35:
    print("you just passed ")
    grade = 0
else:
    print("you failed")
    grade = 0

