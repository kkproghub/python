import flask
from flask import request, jsonify
import mysql.connector
from mysql.connector import Error


app = flask.Flask(__name__)
app.config["DEBUG"] = True
data = []
try:
    connection = mysql.connector.connect(host='localhost',database='employee',user='root',password='root-password',auth_plugin='mysql_native_password')
    myquery = "select * from emp"
    cursor = connection.cursor()
    result = cursor.execute(myquery)
    records = cursor.fetchall()
    for row in records:
        data.append({"id":row[0],"name":row[1],"salary":row[2]})


except mysql.connector.Error as error:
    print("Failed to fetch table in MySQL: {}".format(error))
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")


@app.route('/', methods=['GET'])
def home():
    return '''<h1>Distant Reading Archive</h1>
<p>A prototype API for distant reading of science fiction novels.</p>'''


# A route to return all of the available entries in our catalog.
@app.route('/api/v1/resources/books/all', methods=['GET'])
def api_all():
    return jsonify(data)

app.run()

