"""
Classes are user defined types.
Classes can have a collection of variables represented together as a structure or definition.
"""

class table:
    def setType(self,materialType):
        self.materialType = materialType
    def getType(self):
        return self.materialType

tbl = table()
tbl.setType("wood")

print(tbl.getType())
