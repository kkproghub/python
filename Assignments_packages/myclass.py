#inheriting one class from another.
#it has a parent child relation.

class student:
    #constructor is the function that executes by default.
    #it is defied as __init__
    def __init__(self):
        print("new student has arrived ")
    def setName(self,param_name):
        self.fullname = param_name
    def getName(self):
        return self.fullname
    def setSubject(self,param_subject):
        self.subject = param_subject

class intern(student):
    def __init__(self,project):
        self.project = project
    def introduce(self):
        print("hi I am " + self.fullname + " my subject is " + self.subject + " and I am working on " + self.project)


