#!/usr/bin/python3


#Scope of variables inside function is local.
#we will use two arguments and put them together in a local string variable.
final = "Allen"
def together(str1,str2):
    final = str1 + " and " + str2
    print(final)

print("now let's test the function ")
first = input("what's the first string?")
second = input("and the second string?")
together(first,second)

