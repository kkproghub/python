import mysql.connector
from mysql.connector import Error

try:
    connection = mysql.connector.connect(host='localhost',database='employee',user='root',password='root-password',auth_plugin='mysql_native_password')
    myquery = "select * from emp"
    cursor = connection.cursor()
    result = cursor.execute(myquery)
    records = cursor.fetchall()
    for row in records:
        print ("%d %s %d"%(row[0],row[1],row[2])) 

except mysql.connector.Error as error:
    print("Failed to fetch table in MySQL: {}".format(error))
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")

