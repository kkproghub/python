#skipping some repetations using continue.
#let's asume that we wish to skip printing any name starting with letter a.
names = ["krishnakant","abhi","allen","sam"]
for name in names:
    if name[0] == "a":
        continue
    print("my name is " + name)
