#writing file uses the same open function.
#the second parameter will be "w" instead of "r"
#we are going to write some more details here.
#We need the date and time on which the feedback was provided.
#There are different modules or libraries.
#Library is also called a package.
#a library is a set of python files called modules.
#every module can contain one or more functions or classes.

import datetime
today = datetime.datetime.now()


wf = open("detailed_feedback","w")
wf.write("feedback provided as on %s "% str(today.day) +"/" + str(today.month) + "/" + str(today.year))    
wf.write("I like this session very much \n")
wf.write("I would give %d rating for this "%10)
wf.close()
print ("feedback saved, look in your current directory for the file")
