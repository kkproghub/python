#writing file uses the same open function.
#the second parameter will be "w" instead of "r"

wf = open("feedback","w")
wf.write("I like this session very much \n")
wf.write("I would give %d out of %d  rating for this "%(10,10))
wf.close()
print ("feedback saved, look in your current directory for the file")
