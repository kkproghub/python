#reading a file is very simple.
#we use open function with "r" as the parameter.

rf = open("content.txt","r")

text = rf.readlines()
for line in text:
    print(line)

